Rails.application.routes.draw do
  get 'home/index'
  get 'home/table'
  get 'home/chart'

  match 'surveys', to: 'home#table', via: :all
  match 'charts', to: 'home#charts', via: :all
  
  root 'home#index'
end
