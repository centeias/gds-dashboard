FROM ruby

EXPOSE 3001

ADD . /code
WORKDIR /code
RUN gem install bundle; \
    bundle install; \
    apt-get update; \
    apt-get install nodejs -y

CMD ["rails", "s", "-b", "0.0.0.0", "-p",  "3001"]
