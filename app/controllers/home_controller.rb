require 'open-uri'

class HomeController < ApplicationController
  def index
    @surveys = load_surveys
  end

  def table
    @symptoms = load_symptoms
    @surveys = load_surveys
    @users = load_survey_users
  end

  def charts
    @surveys = load_surveys
    @users = load_users

    @total_symptoms = @surveys.count
    @total_syndromes = Hash.new(0)
    @total_symptoms_by_day = Hash.new(0)
    @total_no_symptoms_by_day = Hash.new(0)

    @surveys.each do |survey|
      day = DateTime.parse(survey["createdAt"]).strftime("%d").to_i

      if survey["no_symptom"] == "N"
        @total_symptoms_by_day[day] += 1
      else
        @total_no_symptoms_by_day[day] += 1
      end
    end

    @total_users = @users.count
    @total_users_by_day = Hash.new(0)

    @users.each do |user|
      day = DateTime.parse(user["createdAt"]).strftime("%d").to_i
      @total_users_by_day[day] += 1
    end
  end

  protected

  def load_symptoms
    api_host=ENV["GDS_API_HOST"] || "localhost"
    api_port=ENV["GDS_API_PORT"] || "80"
    symptoms_json = ActiveSupport::JSON.decode(open("http://#{api_host}:#{api_port}/symptoms/get").read)

    symptoms = {}

    symptoms_json['data'].each do |sympton|
      symptoms[sympton['code']] = sympton['name']
    end

    symptoms
  end

  def load_surveys
    api_host=ENV["GDS_API_HOST"] || "localhost"
    api_port=ENV["GDS_API_PORT"] || "80"
    json = ActiveSupport::JSON.decode(open("http://#{api_host}:#{api_port}/surveys/a").read)

    # json['data'].each do |survey|
    #   survey["info"] = DateTime.parse(survey["createdAt"]).strftime("%d/%m/%Y").to_s
    #
    #
    #   symptoms = check_symptoms survey
    #   syndromes = check_syndromes survey
    #
    #   if symptoms.length > 0
    #     survey["symptoms"] = symptoms
    #     survey["info"] << " - " + symptoms
    #   end
    #
    #   if syndromes.length > 0
    #     survey["syndromes"] = syndromes
    #     survey["info"] << " - " + syndromes
    #   end
    #
    # end

    return json["data"]
  end

  def load_survey_users
    api_host=ENV["GDS_API_HOST"] || "localhost"
    api_port=ENV["GDS_API_PORT"] || "80"
    json = ActiveSupport::JSON.decode(open("http://#{api_host}:#{api_port}/users/a_with_ids").read)

    return json["data"]
  end

  def load_users
    api_host=ENV["GDS_API_HOST"] || "localhost"
    api_port=ENV["GDS_API_PORT"] || "80"
    json = ActiveSupport::JSON.decode(open("http://#{api_host}:#{api_port}/users/a").read)

    return json["data"]
  end

  def check_symptoms(survey)
    symptoms = []

    symptoms << "Febre" if survey.key?("FEBRE")
    symptoms << "Tosse" if survey.key?("TOSSE")
    symptoms << "Dor Garganta" if survey.key?("DORGARGA")
    symptoms << "Falta de Ar" if survey.key?("FALTAAR")
    symptoms << "Nausea e Vômito" if survey.key?("NAUSVOM")
    symptoms << "Diarreia" if survey.key?("DIARREIA")
    symptoms << "Sangramento" if survey.key?("SANGRAME")
    symptoms << "Manchas Vermelhas" if survey.key?("MANVERM")
    symptoms << "Dor no Corpo" if survey.key?("DORCORPO")
    symptoms << "Dor Juntas" if survey.key?("DORJUNTAS")
    symptoms << "Dor de Cabeça" if survey.key?("DORCABECA")
    symptoms << "Coceira" if survey.key?("COCEIRA")
    symptoms << "Olhos Vermelhos" if survey.key?("OLHOSVERM")

    return symptoms.join(', ')
  end

  def check_syndromes(survey)
    syndromes = []

    syndromes << "Exantematica" if survey["exantematica"] == true
    syndromes << "Diarreica" if survey["diarreica"] == true
    syndromes << "Respiratoria" if survey["respiratoria"] == true

    return syndromes.join(', ')
  end
end
